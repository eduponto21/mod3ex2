/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Pratica32 {
    
    public static void main(String[] args) {
        double d = densidade(-1, 67, 3);
        System.out.println(d);
    }

    public static double densidade(double x, double media, double desvio) {
        double d = Math.exp(-0.5*Math.pow(((x - media) / desvio), 2))/(Math.sqrt(2 * Math.PI) * desvio);//expressão para cálculo da densidade;

        return d;
    }
}
